mkdir -p ./dist/firefox/aau-enhancements
rm -r ./dist/firefox/aau-enhancements/*

cp manifest-firefox.json dist/firefox/aau-enhancements/manifest.json
cp -r -t ./dist/firefox/aau-enhancements ./images ./scripts icon.png icon128.png LICENSE popup.css popup.html

cd dist/firefox/aau-enhancements
zip -r aau-enhancements.zip .
