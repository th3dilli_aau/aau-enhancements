mkdir -p ./dist/chrome/aau-enhancements
rm -r ./dist/chrome/aau-enhancements/*

cp manifest-chrome.json dist/chrome/aau-enhancements/manifest.json
cp -r -t ./dist/chrome/aau-enhancements ./images ./scripts icon.png icon128.png LICENSE popup.css popup.html

cd dist/chrome/aau-enhancements
zip -r aau-enhancements.zip .