chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
  if (message.function == "skipTime") {
    skipTime(message.amount);
  } else if (message.function == "setSpeed") {
    setPlaybackRate(message.amount);
  }
});

function setPlaybackRate(rate) {
  let video = document.querySelector("#vjs_video_3_html5_api");
  video.playbackRate = rate;
}

function vidPause() {
  let main = document.querySelector("#vjs_video_3_html5_api");
  if (main) {
    if (main.paused) {
      main.play();
    } else {
      main.pause();
    }
  }
}

function skipTime(time) {
  let main = document.querySelector("#vjs_video_3_html5_api");
  main.duration;
  if (main.duration < main.currentTime + time) {
    main.currentTime = main.duration;
  } else if (main.currentTime + time < 0) {
    main.currentTime = 0;
  } else {
    main.currentTime = main.currentTime + time;
  }
}
function onKeyDown(e) {
  switch (e.keyCode) {
    case 76: {
      // l
      skipTime(10);
      break;
    }
    case 32: // space
    case 75: {
      // k
      vidPause();
      break;
    }
    case 74: {
      // j
      skipTime(-10);
      break;
    }
    case 39: {
      // ->
      skipTime(5);
      break;
    }
    case 37: {
      // <-
      skipTime(-5);
      break;
    }

    default:
    // console.log("key: " + e.keyCode);
  }
}
document.addEventListener("keydown", onKeyDown, false);
