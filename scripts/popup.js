let volume_slider;
let lvList;
let lvListTxt;

document.addEventListener("DOMContentLoaded", () => {
  let playbackspeed_reset = document.getElementById("playbackspeed-reset");
  let speed_rev_big = document.getElementById("speed-rev-big");
  let speed_rev = document.getElementById("speed-rev");
  let speed_for = document.getElementById("speed-for");
  let speed_for_big = document.getElementById("speed-for-big");

  //settings
  lvList = document.getElementById("lvlist");
  lvListTxt = document.getElementById("lvlistTxt");
  volume_slider = document.getElementById("volume-slider");

  speed_rev_big.addEventListener("click", () => sendSkipTime(-10));
  speed_rev.addEventListener("click", () => sendSkipTime(-5));
  speed_for.addEventListener("click", () => sendSkipTime(5));
  speed_for_big.addEventListener("click", () => sendSkipTime(10));

  //settings
  lvList.addEventListener("click", onLVList);
  lvListTxt.addEventListener("keyup", lvListeOnKeyup);
  volume_slider.addEventListener("input", onVolumeChange);

  //load settings
  loadSettings();
});

function loadSettings() {
  chrome.storage.sync.get((result) =>
    chrome.runtime.sendMessage({ function: "log", log: result })
  );

  chrome.storage.sync.get("aau_redirect", (result) => {
    if (result.aau_redirect) {
      lvList.checked = true;
    }
  });

  chrome.storage.sync.get("aau_bbb_volume", (result) => {
    if (result.aau_bbb_volume) {
      volume_slider.value = result.aau_bbb_volume;
    }
  });

  chrome.storage.sync.get("aau_redirect_sem", (result) => {
    if (result.aau_redirect_sem) {
      lvListTxt.value = result.aau_redirect_sem;
    }
  });
}

function onVolumeChange() {
  sendToPage({
    function: "setVolume",
    amount: Number.parseFloat(volume_slider.value),
  });
  chrome.storage.sync.set({ aau_bbb_volume: volume_slider.value });
}

function onLVList() {
  if (lvList.checked) {
    // addRedirect();
    chrome.runtime.sendMessage({ function: "addRedirect" });
  } else {
    // removeListener();
    chrome.runtime.sendMessage({ function: "removeRedirect" });
  }
  chrome.storage.sync.set({ aau_redirect: lvList.checked });
}

function lvListeOnKeyup() {
  chrome.storage.sync.set({ aau_redirect_sem: lvListTxt.value.toUpperCase() });
  chrome.runtime.sendMessage({ function: "updateRedirect" });
}

function sendSetSpeed(amount) {
  sendToPage({ function: "setSpeed", amount });
}

function sendSkipTime(amount) {
  sendToPage({ function: "skipTime", amount });
}

function sendToPage(payload) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    chrome.tabs.sendMessage(tabs[0].id, payload);
  });
}
