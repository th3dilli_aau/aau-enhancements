let semester = "21W";
function listener(requestDetails) {
  return {
    redirectUrl: `${requestDetails.url}?sem=${semester}`,
  };
}

function addRedirect() {
  chrome.webRequest.onBeforeRequest.addListener(
    listener,
    {
      urls: [
        "https://campus.aau.at/sp/ctl/lehrveranstaltung/liste",
        "https://campus.aau.at/sp/ctl/pruefung/liste",
      ],
    },
    ["blocking"]
  );
}

function removeRedirect() {
  if (chrome.webRequest.onBeforeRequest.hasListener(listener)) {
    chrome.webRequest.onBeforeRequest.removeListener(listener);
  }
}

function updateRedirect() {
  chrome.storage.sync.get("aau_redirect_sem", (result) => {
    semester = result.aau_redirect_sem;
  });
}

chrome.storage.sync.get("aau_redirect", (result) => {
  if (result.aau_redirect) {
    chrome.storage.sync.get("aau_redirect_sem", (result) => {
      semester = result.aau_redirect_sem;
      addRedirect();
    });
  } else if (result.aau_redirect === undefined) {
    chrome.storage.sync.set({ aau_redirect: false });
  } else if (!result.aau_redirect) {
    removeRedirect();
  }
});

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.function === "addRedirect") {
    addRedirect();
  } else if (request.function === "removeRedirect") {
    removeRedirect();
  } else if (request.function === "updateRedirect") {
    updateRedirect();
  } else if (request.function === "log") {
    //chrome.runtime.sendMessage({ function: "log", log: "Hello log" });
    console.log(request.log);
  }
});
