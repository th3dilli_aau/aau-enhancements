chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
  if (message.function == "setVolume") {
    setVolume(message.amount);
  }
});

function setVolume(amount) {
  let audio = document.getElementById("remote-media");
  if (audio) {
    audio.volume = amount;
  }
}

chrome.storage.sync.get("aau_bbb_volume", (result) => {
  if (result.aau_bbb_volume) {
    setVolume(result.aau_bbb_volume);
  }
});

// document.getElementsByTagName("audio")[0].src
// "" or "https://bbb01.aau.at/html5client/resources/sounds/silence.mp3"

// document.getElementsByTagName("audio")[0].paused
// true or false

// document.getElementsByTagName("audio")[0].srcObject
// null or
// srcObject: MediaStream
// - active: true
// - id: "982a076f-59d1-4aab-a5cb-d1658f9c3a54"
// - onactive: null
// - onaddtrack: null
// - oninactive: null
// - onremovetrack: null
