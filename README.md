# AAU Enhancements

Is a extension for the Alpen-Adria-Universitys BigBlueButton video player and the campus.aau.at site.
This extension is available for chrome/chromium-browsers and firefox.

![](images/preview.png)

# Features

- skip time forward backwards in bbb videos (youtube controls)
- automatically redirect to semester you would like on the courses page and exams page (setting for it in extension window)
- change volume of live bbb sessions (volume setting is saved and automatically restored for new sessions)

# Keyboard shortcuts

| Key            | function                 |
| -------------- | ------------------------ |
| k, space       | pause/play               |
| j, left arrow  | skip back 10s and 5 s    |
| l, right arrow | skip forward 10s and 5 s |

# Installation

## Chrome / Brave / Chromium / Edge

[Chrome Webstore](https://chrome.google.com/webstore/detail/aau-enhancements/eppanleeaddhdakgjjkjefaofkkdmphd)

## Firefox

Download [aau_enhancements-1.3-fx.xpi](https://gitlab.com/th3dilli_aau/aau-enhancements/-/raw/main/firefox/aau_enhancements-1.3-fx.xpi)

This will automatically update for future updates. No need to redownload and install.

# Discord

Join our [Discord](https://discord.gg/fQAXxdG)
